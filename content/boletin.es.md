---
description: Boletín de MexFlux
title: Boletín de MexFlux
---

{{< lead >}}
El boletín es nuestro mas reciente esfuerzo para comunicarnos y hacer difusión de lo que pasa en nuestra comunidad. En cada volumen, la comunidad de MexFlux anunciara sus actividades mas recientes y futuras, y también es un espacio para que la comunidad comparta fotos, notas de campo, ideas, proyectos y anuncios. Por ahora, solo estaremos publicando la versión en español del boletín.
{{< /lead >}}

Haz clic en los siguientes enlaces para descargar los volúmenes:

[Volumen 2 (enero 2025)](https://drive.google.com/file/d/1-1EdtRgPloPQiQLWxl0HFlOYvHarbyM6/view?usp=sharing)

[Volumen 1 (noviembre 2024)](https://drive.google.com/file/d/1zMqg5H2aBlQ_dFq1A_BAMJ9jxGUVdHi2/view?usp=sharing)
