---
title: "Amigos de MexFlux: Dulce Y. Flores Rentería"
date: 2023-06-24T11:36:00-07:00
categories: ["amigosDeMexFlux"]
tags: ["amigosDeMexFlux"]
---

{{< lead >}}
Dulce Y. Flores Rentería es profesora investigadora de Conahcyt comisionada al Centro de Investigación y de Estudios Avanzados del Instituto Politécnico Nacional (CINVESTAV), Saltillo, Coahuila. Ha trabajado en los sitios San Isidro y General Cepeda en Coahuila, México.
{{< /lead >}}

![](../../../img/amigos-de-mexflux/dulce-flores-small.jpg)

**¿Cuáles son las líneas de investigación que persigues en tu grupo de trabajo?**

Analizar el intercambio de carbono del suelo/ecosistema a la atmósfera como indicador de su degradación. Monitoreo de prácticas agrícolas y ganaderas para evaluar su grado de sustentabilidad. 


**¿Qué proyecto realizas y/o participas actualmente?**

- Monitoreo de los indicadores de calidad del suelo y productividad de pastizales ganaderos en el desierto Chihuahuense para la evaluación de su sustentabilidad.
- Evaluación de la evapotranspiración de campos agrícolas al sureste del estado de Coahuila para mejorar la gestión del riego aplicado. 


**Cuéntanos brevemente cómo o cuándo empezaste a trabajar en el estudio de flujos ecosistémicos.**

Comencé a trabajar con la respiración del suelo con cámaras dinámicas durante mis estudios de doctorado en la península Ibérica. Posteriormente, ya como investigadora, incorporé el estudio de flujos ecosistémicos en mi línea de investigación.


**¿Qué trabajo o proyecto te llevó a pensar en flujos?**

Fue con un proyecto a demanda del Instituto Nacional de Estadística y Geografía, cuyo objetivo fue desarrollar una metodología para la evaluación de la degradación biológica del suelo en ecosistemas áridos y semiáridos de México. El proyecto contempló el monitoreo de la respiración del suelo en distintos sitios con diferentes usos de suelo durante un año, así como la medición de flujos ecosistémicos y una serie de indicadores de la calidad del suelo. 


**¿Qué descubrimiento de tu(s) estudio(s)/proyecto(s) ha sido más interesante, sorprendente e inesperado? (compartir ligas a papers)**
Desarrollamos la metodología para la determinación del grado de degradación biológica del suelo usando el intercambio de carbono como el principal indicador. En otros proyectos hemos evaluado la evapotranspiración de campos agrícolas al sureste del estado de Coahuila para mejorar la gestión de riego. 

- https://doi.org/10.1016/j.scitotenv.2022.159918
- https://doi.org/10.1007/s42729-023-01147-0
- https://peerj.com/articles/14542/
- https://doi.org/10.28940/terra.v39i0.1251


**¿Qué haces por diversión/hobby vida más allá de la academia?**

Me encanta hacer divulgación de la ciencia, cuidar de las plantas en casa y pasar tiempo de calidad con mi hija y mi esposo, jugar juegos de mesa y armar rompecabezas. 

