---
title: "Amigos de MexFlux: Carlo Alberto Domínguez Eusebio"
date: 2024-12-04T22:00:00-07:00
categories: ["amigosDeMexFlux"]
tags: ["amigosDeMexFlux"]
---

{{< lead >}}
Carlo is a postdoctoral researcher at Instituto de Ecología (INECOL) currently working on muti-site syntheses and soon will install instrumentation in a coffee plantation.
{{< /lead >}}

![](../../img/amigos-de-mexflux/carlo-dominguez-small.jpg)


**Which are the lines of research of your working group?**

Estimation of gross primary productivity in tropical ecosystems.


**Which project are you currently working on and participating in?**

Net carbon exchange between the biosphere and atmosphere, and a review of emission factors for Mexican ecosystems.


**What Do You Love About Your Project?**

Using instruments, models, and remote sensors to estimate primary productivity.


**Tell us briefly how or when you started working on the study of ecosystem fluxes.**

At the end of my undergraduate studies and the beginning of my doctoral program, I had the opportunity to operate eddy covariance instruments to measure surface energy balance in a cloud forest.


**What study or project lead you to think about fluxes?**

Since my undergraduate studies, I’ve been interested in understanding how the atmosphere interacts with the surface, including vegetation and living organisms.

**Which article or talk inspired you to work on what you do?**

The book The Urban Energy Balance by Oke.


**Which is the most interesting, surprising or unexpected discovery of your study/studies or project(s) (share links to papers)?**

The urbanization of cloud forests alters energy distribution, leading to faster air warming and increased evapotranspiration rates.
https://doi.org/10.1016/j.ufug.2019.03.018


**What do you do for fun or a hobby beyond the academy?**

I enjoy technology and video games.
