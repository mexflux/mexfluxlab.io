---
title: "Amigos de MexFlux: Aylin Cristina Barreras Apodaca"
date: 2022-10-17T19:00:00-07:00
categories: ["amigosDeMexFlux"]
tags: ["amigosDeMexFlux"]
---

{{< lead >}}
Aylin es consultora en la Cooperación Técnica Forestal México - Estados Unidos
{{< /lead >}}

![](../../../img/amigos-de-mexflux/aylin-barreras-small.jpg)

**¿Cuáles son las líneas de investigación que persigues en tu grupo de trabajo?**

Cuantificación del budget de carbono en ecosistemas terrestres (i.e., forestales, agricolas, etc.) y mapeo digital de carbono en suelos y vegetación a partir del aprendizaje automático asistido por computadora (Machine Learning).

**¿Qué proyecto realizas y/o participas actualmente?**

Mapeo digital de rasgos (i.e., biomasa, carbono almacenado, altura de árboles) y funciones ecológicas en los bosques de México.

**Cuéntanos brevemente cómo o cuándo empezaste a trabajar en el estudio de flujos ecosistémicos.**

Tuve la oportunidad de trabajar con datos de flujos de CO2 en manglar cuando estaba en los últimos semestres de mi licenciatura. El trabajo que realicé fue presentado en la Unión Geofísica Mexicana. De ahí, en la maestría trabajé con flujos de CO2 en una pradera de pastos marinos/laguna costera.

**¿Qué trabajo o proyecto te llevó a pensar en flujos?**

El gran potencial de mitigación de CO2 atmosférico en ecosistemas de manglar

**¿Qué descubrimiento de tu(s) estudio(s)/proyecto(s) ha sido más interesante, sorprendente e inesperado? (compartir ligas a papers)**

Tasas altas de intercambio de CO2 aire-agua sobre praderas de pastos marinos
(https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2020GB006848)

**¿Qué haces por diversión/hobby vida más allá de la academia?**

Salir a pasear con mi perrita lola, escuchar podcasts de mujeres feministas e ilustrar infografías infantiles sobre temas ambientales.

