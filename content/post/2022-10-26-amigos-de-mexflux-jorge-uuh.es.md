---
title: "Amigos de MexFlux: Jorge Maximiliano Uuh Sonda"
date: 2022-10-26T13:00:00-07:00
categories: ["amigosDeMexFlux"]
tags: ["amigosDeMexFlux"]
---

{{< lead >}}
Jorge es Posdoctorante CONACYT en el Instituto Tecnológico de Sonora (ITSON). Ha trabajado en los sitios de El Palmar, Yucatán (TDF); Ejido Sisal, Yucatán (Mangrove).
{{< /lead >}}

![](../../../img/amigos-de-mexflux/jorge-uuh-small.jpg)

**¿Cuáles son las líneas de investigación que persigues en tu grupo de trabajo?**

Ecohidrología de Sistemas Tropicales; Covarianza de Vórtices; Percepción Remota

**¿Qué proyecto realizas y/o participas actualmente?**

En mi proyecto Posdoctoral: Monitoreo del intercambio de flujos de agua, carbono y energía entre el manglar y la atmósfera como estrategia de resiliencia y adaptación al cambio climático en el Ejido Sisal, Yucatán

**Cuéntanos brevemente cómo o cuándo empezaste a trabajar en el estudio de flujos ecosistémicos**

Fue durante el Doctorado, y como siempre comento, llegué de una manera muy afortunada y poco convencional. En la maestría estaba trabajando en cosas completamente diferentes, en Astrofísica. Diversas razones, personales y profesionales me hicieron cuestionarme si seguir por esa línea o regresar a mis orígenes de Ingeniero, con trabajos de campo y manipulación de instrumentos. En la búsqueda de opciones, encontré un excelente Instituto de Ingeniería, muy cerca de mi ciudad natal, y cuyas líneas de investigación me permitieron trabajar en otro tema que me apasionaba, la Ciencias de la Tierra. Fue así como pude vincularme con líneas de investigación relacionada a la Ecohidrología y al estudio de las Dinámicas de Flujos Ecosistémicos mediante la técnica de Covarianza de Vórtices.   

**¿Qué trabajo o proyecto te llevó a pensar en flujos?**

Justamente mi proyecto Doctoral. Este consistió en instalar, implementar y manejar un sistema de Covarianza de Vórtices para estudiar la Dinámica Temporal de Fflujos Ecosistémicos de la Selva Baja Caducifolia, localizada al Noroeste de la Península de Yucatán, y cuyo equilibrio y extensión se han visto amenazados en los últimos años por desarrollos urbanos y comerciales. 

**¿Qué descubrimiento de tu(s) estudio(s)/proyecto(s) ha sido más interesante, sorprendente e inesperado? (compartir ligas a papers)**

Lo novedoso de los estudios en los que he participado, es que hemos empezado a entender y describir, desde el punto de vista de estas dinámicas de flujos, el comportamiento de los diferentes ecosistemas de la Península de Yucatán. Muy poco se sabía sobre las interacciones Ecohidrológicas de estos biomas, su variabilidad intra e interanual, la relación con la variabilidad climática y su dependencia temporal con el agua subterránea.    

https://onlinelibrary.wiley.com/doi/abs/10.1002/hyp.13230
https://agupubs.onlinelibrary.wiley.com/doi/abs/10.1029/2019JG005629

**¿Qué haces por diversión/hobby vida más allá de la academia?**

Disfruto mucho ver series de TV, especialmente cómicas, ir al cine o salir a cenar con mi prometida Aurea, cuando hay oportunidad beber una buena cerveza artesanal con amigos o familia, así como jugar juegos de mesa. En cuestión de deportes me gusta el football y el squash. Finalmente, amo viajar y conocer nuevos lugares.
