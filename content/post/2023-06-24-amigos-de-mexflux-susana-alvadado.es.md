---
title: "Amigos de MexFlux: Susana Alvarado Barrientos"
date: 2023-06-24T12:30:00-07:00
categories: ["amigosDeMexFlux"]
tags: ["amigosDeMexFlux"]
---

{{< lead >}}
Susana es investigadora titular en el Instituto de Ecología A.C. (INECOL), in Xalapa, Veracruz. Ha trabajado en los sitios MX-PMm y MX-Ord.
{{< /lead >}}

![](../../../img/amigos-de-mexflux/susana-alvarado-small.jpg)


**¿Cuáles son las líneas de investigación que persigues en tu grupo de trabajo?**

Mi trabajo de investigación y docencia se enmarca en la ecohidrología y las interacciones ecosistema-atmósfera, es decir, el intercambio de agua, carbono y energía en la zona crítica del planeta. Me interesa conocer el funcionamiento básico de los ecosistemas para entender los efectos biofísicos de modificaciones antropogénicas tales como los cambios en el uso del suelo, y el desarrollo de la restauración ecológica de ecosistemas degradados. Me enfoco en estudiar ambientes tropicales húmedos y de montaña.


**¿Qué proyecto realizas y/o participas actualmente?**

Actualmente estudiamos los flujos de carbono suelo-atmósfera en ecosistemas que están siendo restaurados (particularmente manglar, bosque mesófilo de montaña y sus zonas riparias). También continuamos estudiando el funcionamiento ecohidrológico de cafetales bajo sombra (particularmente el consumo de agua de distintas variedades y arreglos estructurales).


**Cuéntanos brevemente cómo o cuándo empezaste a trabajar en el estudio de flujos ecosistémicos.**

Me interesó mucho desde mis primeras clases de maestría. Me pareció maravilloso y casi mágico que podamos medir algo invisible y al mismo tiempo tan fundamental para la vida como el intercambio de agua, energía, y carbono entre la biota y la atmósfera. Luego, tuve la oportunidad de subir una torre de flujos, de 40 m en la selva Amazónica, y allí me nacieron las ganas de participar en este tipo de investigaciones y contribuir a la red mundial de personas que se dedican a conocer el funcionamiento básico de los ecosistemas. Realicé el doctorado estudiando la transpiración de árboles plantados con sensores de flujo de savia, y la técnica de eddy covariance la apliqué durante el posdoctorado contrastando el balance de energía de tres usos de suelo en la zona de bosque nuboso. Hasta entonces sólo había trabajado con flujos de agua y energía, y no fue hasta que obtuve mi primer proyecto de infraestructura como jóven investigadora, que apliqué la técnica para conocer cómo cambian los flujos de carbono de un manglar cuando no está inundado.

**¿Qué trabajo o proyecto te llevó a pensar en flujos?**

Más que trabajo o proyecto, llegué a pensar en flujos por mi afición de observar nubes y niebla en el bosque nuboso, y el interés por entender mejor el vínculo bosque-agua. Este interés me llevó a cursar una maestría en ecohidrología en Amsterdam porque tenían proyectos sobre estos temas, y es allí donde todo empezó a “fluir” :)  
 

**¿Qué descubrimiento de tu(s) estudio(s)/proyecto(s) ha sido más interesante, sorprendente e inesperado? (compartir ligas a papers)**

De mi trabajo con la torre de flujos en un manglar interior, fue interesante observar que durante el período más prolongado cuando el manglar no estuvo inundado éste actuó como fuente de carbono hacia la atmósfera. Lo más sorprendente fue que durante ese período la asimilación de carbono disminuyó pero no así la evapotranspiración, es decir que observamos un desacoplamiento del intercambio de carbono y agua, lo cual indica condiciones extremas negativas para el ecosistema de manglar.  
https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2020JG005811


**¿Qué haces por diversión/hobby vida más allá de la academia?**

Por diversión salgo a caminar por parques y alrededores de Xalapa, me gusta mucho viajar e ir a museos, leer y escuchar música. Además, me gusta pasar tiempo divertido con las ocurrencias de mi hijo Lucas.

