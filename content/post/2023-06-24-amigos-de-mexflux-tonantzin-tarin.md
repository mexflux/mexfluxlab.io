---
title: "Amigos de MexFlux: Tonantzin Tarin Terrazas"
date: 2023-06-24T16:15:00-07:00
categories: ["amigosDeMexFlux"]
tags: ["amigosDeMexFlux"]
---

{{< lead >}}
Tonantzin is a researcher Instituto de Ecología - UNAM.
{{< /lead >}}

![](../../img/amigos-de-mexflux/tonantzin-tarin-small.png)


**Which are the lines of research of your working group?**

The lines of research that I pursue are within the framework of ecophysiology and ecohydrology of semiarid ecosystems. I am very interested in studying the efficiency of water use of vegetation and ecosystems, that is, conserving water costs vegetation to assimilate carbon. This is directly related to plant-ecosystem and atmosphere interactions and contributes to generating adequate information on the basic functioning of ecosystems.


**Which project are you currently working on and participating in?**

I am currently working on studying water use strategies in coexisting species in arid and semiarid zones.


**Tell us briefly how or when you started working on the study of ecosystem flows.**

My interest was born with water, and I began to study the flow of evapotranspiration in a xerophytic scrubland in Sonora. I did my bachelor's and master's studies using the combination of stable isotopes of water with direct water flow detection with the eddy covariance technique. These data will help us identify the percentage of water corresponding to the vegetation's transpiration. Later I got involved in carbon, and currently, I am very interested in studies of the efficiency of water use.


**What study or project lead you to think about fluxes?**

It was when we tried to understand the components of the water cycle better, and then I got involved in evapotranspiration studies.
 

**Which is the most interesting, surprising or unexpected discovery of your study/studies or project(s) (share links to papers)?**

It contributed to understanding and evaluating how evapotranspiration components vary according to precipitation pulses. In addition, we studied and discovered interesting information on the efficiency of water use in vegetation and ecosystems in arid and semiarid zones.


**What do you do for fun or a hobby beyond the academy?**

I love to run, race and hike. Right now, I really enjoy my days with Nicolas, my baby.
