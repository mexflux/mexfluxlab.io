---
title: "Amigos de MexFlux: Tonantzin Tarin Terrazas"
date: 2023-06-24T16:15:00-07:00
categories: ["amigosDeMexFlux"]
tags: ["amigosDeMexFlux"]
---

{{< lead >}}
Zulia es profesora investigadora en el Instituto Tecnológico de Sonora. Ha trabajado en los sitios Estero El Soldado (laguna costera) y Sisal (manglar)
{{< /lead >}}

![](../../../img/amigos-de-mexflux/tonantzin-tarin-small.png)


**¿Cuáles son las líneas de investigación que persigues en tu grupo de trabajo?**

Las líneas de investigación que persigo son el marco de la ecofisiología y ecohidrología de ecosistemas semiáridos. Me interesa mucho estudiar la eficiencia del uso del agua de la vegetación y los ecosistemas, es decir, cuánta agua le cuesta a la vegetación asimilar carbono. Esto se relaciona directamente con las interacciones planta-ecosistema y atmósfera y contribuye para generar información oportuna sobre el funcionamiento básico de los ecosistemas. 


**¿Qué proyecto realizas y/o participas actualmente?**

Actualmente estoy trabajando en estudiar las estrategias en el uso de agua en especies coexistentes de zonas áridas y semiáridas


**Cuéntanos brevemente cómo o cuándo empezaste a trabajar en el estudio de flujos ecosistémicos.**

Mi interés nació con el agua, empecé a estudiar el flujo de la evapotranspiración en un matorral xerófilo de Sonora. Realicé mis estudios de licenciatura y maestría utilizando la combinación de isótopos estables de agua con mediciones directas de flujos de agua con la técnica de eddy covariance. Estos datos nos ayudaron a identificar el porcentaje de agua que corresponde a la transpiración de la vegetación. Después me fui involucrando en el carbono y actualmente me interesan mucho los estudios de la eficiencia del uso del agua. 


**¿Qué trabajo o proyecto te llevó a pensar en flujos?**

Fue cuando intentamos entender mejor los componentes del ciclo del agua y me involucre en estudios de evapotranspiración
 

**¿Qué descubrimiento de tu(s) estudio(s)/proyecto(s) ha sido más interesante, sorprendente e inesperado? (compartir ligas a papers)**

He contribuido a entender y evaluar cómo varían los componentes de la evapotranspiración según los pulsos de precipitación. Además he estudiado y descubierto información interesante de la eficacia del uso del agua tanto en la vegetación como en los ecosistemas de zonas áridas y semiáridas. 


**¿Qué haces por diversión/hobby vida más allá de la academia?**

Me encanta correr, participar en carreras y hacer senderismo. Ahora mismo disfruto mucho de mis días con Nicolas mi bebé.
