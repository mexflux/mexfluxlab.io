---
title: "Amigos de MexFlux: Dra. Zulia Mayari Sánchez Mejía"
date: 2023-06-24T16:00:00-07:00
categories: ["amigosDeMexFlux"]
tags: ["amigosDeMexFlux"]
---

{{< lead >}}
Zulia es profesora investigadora en el Instituto Tecnológico de Sonora. Ha trabajado en los sitios Estero El Soldado (laguna costera) y Sisal (manglar)
{{< /lead >}}

![](../../../img/amigos-de-mexflux/zulia-sanchez-small.jpg)


**¿Cuáles son las líneas de investigación que persigues en tu grupo de trabajo?**

Interacciones superficie de la Tierra -  atmósfera. Me parece fascinante aprender sobre la partición de energía y el desarrollo de la capa límite planetaria. También me interesan las interacciones suelo-planta-atmósfera, por ejemplo como la humedad en el suelo esta disponible para las plantas y como a su vez la cobertura vegetal influye en la partición de energía, u otros procesos ecohidrológicos a través de su fenología y dosel. 


**¿Qué proyecto realizas y/o participas actualmente?**

Actualmente colidero un Observatorio Ecohidrológico en el manglar de Sisal, con preguntas encaminadas a conocer su papel en los flujos verticales de CO2. También colidero un observatorio ecohidrológico en el Valle del Yaqui en un cultivo de trigo, con preguntas enfocadas en el uso eficiente del agua y los controladores del aporte de partículas erosionables de suelo a la atmósfera. También participo en un estudio a largo plazo para monitorear los efectos de condiciones climáticas extremas en el bosque tropical seco, particularmente en este momento contribuimos con la observación del movimiento de suelo (erosión) ya sea pérdida o acreción en un gradiente de disturbio generado por la helada del 2011.  


**Cuéntanos brevemente cómo o cuándo empezaste a trabajar en el estudio de flujos ecosistémicos.**

Tuve mi primera experiencia con el tema cuando estaba en la maestría, aunque no fue hasta el doctorado que realmente me sumergí en el estudio de los flujos entre el ecosistema y la atmósfera. 
Para mí es muy emocionante detectar patrones que cambian en tiempo y espacio, así como conectar las interacciones que controlan esos patrones. 


**¿Qué trabajo o proyecto te llevó a pensar en flujos?**

El doctorado, trabajar en el Desierto Sonorense para mí fue un placer, desde que era niña sentía una gran curiosidad por la naturaleza y me impresionaba cuando veía en documentales de como plantas y animales se adaptan a condiciones extremas como en los desiertos. El hecho de que los procesos ecohidrológicos estén controlados por pulsos de humedad, y que la entrada del agua desencadene múltiples reacciones me parece algo que vale la pena analizar, especialmente en el contexto de cambio climático. 


**¿Qué descubrimiento de tu(s) estudio(s)/proyecto(s) ha sido más interesante, sorprendente e inesperado? (compartir ligas a papers)**

- Las interacciones de la superficie con la capa límite planetaria es un artículo que disfrute realizar y aprendí mucho  https://agupubs.onlinelibrary.wiley.com/doi/pdfdirect/10.1002/2016WR020275

- La conexión con la capa límite planetaria es algo que debe ser abordado con un enfoque colaborativo de varias disciplinas 
https://agupubs.onlinelibrary.wiley.com/doi/abs/10.1029/2021JG006707

- Y ese mismo enfoque colaborativo aplica para aprender la contribución en los flujos de Carbono de ecosistemas retadores y poco estudiados como los pastos marinos. 
https://agupubs.onlinelibrary.wiley.com/doi/pdfdirect/10.1029/2020GB006848


**¿Qué haces por diversión/hobby vida más allá de la academia?**

Disfruto mucho cocinar y estoy aprendiendo de mixología. Aunque no es constante trato de realizar actividades que ayuden a mi equilibrio emocional y salud mental como yoga, y compartir tiempo con mi pareja y perritas. 
