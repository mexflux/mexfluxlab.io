---
title: "Amigos de MexFlux: Dulce Y. Flores Rentería"
date: 2023-06-24T11:36:00-07:00
categories: ["amigosDeMexFlux"]
tags: ["amigosDeMexFlux"]
---

{{< lead >}}
Dulce is a research professor from Conahcyt commissioned to Center for Research and Advanced Studies of the National Polytechnic Institute (CINVESTAV), Saltillo, Coahuila. She has worked at the sites San Isidrio y General Cepeda in Coahuila, México.
{{< /lead >}}

![](../../img/amigos-de-mexflux/dulce-flores-small.jpg)

**Which are the lines of research of your working group?**

Analyze the carbon exchange from the soil/ecosystem to the atmosphere as an indicator of its degradation. We are monitoring agricultural and livestock practices to assess their degree of sustainability.


**Which project are you currently working on and participating in?**

We are monitoring soil quality indicators and the productivity of livestock pastures in the Chihuahuan desert to assess their sustainability. Also, we are evaluating of the evapotranspiration of agricultural fields to the southeast of the state of Coahuila to improve the management of applied irrigation.


**Briefly tell us how or when you started working on ecosystem fluxes.**

I started working with soil respiration with dynamic cameras during my doctoral studies in the Iberian Peninsula. Later, as a researcher, I incorporated the study of ecosystem flows into my line of research.


**What study or project lead you to think about fluxes?**

It was with a project at the request of the National Institute of Statistics and Geography, whose objective was to develop a methodology to evaluate the biological degradation of the soil in arid and semi-arid ecosystems in Mexico. The project contemplated monitoring soil respiration in different sites with different land uses for one year, as well as measuring ecosystem flows and a series of soil quality indicators.


**Which is the most interesting, surprising or unexpected discovery of your study/studies or project(s) (share links to papers)?**
We developed the methodology for determining the degree of biological degradation of the soil using carbon exchange as the leading indicator. In other projects, we have evaluated the evapotranspiration of agricultural fields in the southeast of the state of Coahuila to improve irrigation management.

- https://doi.org/10.1016/j.scitotenv.2022.159918
- https://doi.org/10.1007/s42729-023-01147-0
- https://peerj.com/articles/14542/
- https://doi.org/10.28940/terra.v39i0.1251


**What do you do for fun or a hobby beyond the academy?**
I love science communication, planting at home, and spending quality time with my daughter and husband, such as playing board games and puzzles.
