---
title: "Amigos de MexFlux: Mónica Cervantes Jiménez"
date: 2023-03-10T18:00:00-07:00
categories: ["amigosDeMexFlux"]
tags: ["amigosDeMexFlux"]
---

{{< lead >}}
Monica es profesora-investigadora de tiempo libre categoría III en la Facultad de Ciencias Naturales de la Universidad Autónoma de Querétaro.
{{< /lead >}}

![](../../../img/amigos-de-mexflux/monica-cervantes-small.jpg)

**¿Cuáles son las líneas de investigación que persigues en tu grupo de trabajo?**

Uso de sensores remotos para modelar flujos de carbono en agroecosistemas.

**¿Qué proyecto realizas y/o participas actualmente?**

Estoy explorando la relación entre la Florescencia Inducida por el Sol (SIF) medida con sensores remotos y la productividad primaria bruta a nivel ecosistema en el semidesierto Queretano. Me interesa mucho el comportamiento de la fenología de la vegetación y su influencia en la captura de carbono y agua. Otro aspecto que me interesa es la aplicación de modelos de aprendizaje automático y sensores remotos en la modelación de sistemas agropecuarios, sobre todo con un enfoque de optimización en el aprovechamiento de recursos naturales para proponer estrategias de sustentabilidad.

**Cuéntanos brevemente cómo o cuándo empezaste a trabajar en el estudio de flujos ecosistémicos.**

Me apasiona la ecología de los agroecosistemas y en la medida en la que me fui involucrando de manera más profunda en la investigación de flujos de carbono, me di cuenta que se desconocen muchos procesos biofísicos y bióticos que se pueden explorar con sensores remotos. Entender el funcionamiento de los ecosistemas cambió mi perspectiva sobre las decisiones que se deben tomar en las unidades de producción para aminorar el impacto ambiental.

**¿Qué trabajo o proyecto te llevó a pensar en flujos?**

Teníamos datos de flujos de carbono de una torre de covarianza de vórtices y empezamos a trabajarlos, al principio leímos algunas publicaciones, pero no entendíamos los métodos de análisis convencional.  Mientras seguimos explorando los datos, encontramos los modelos de aprendizaje automático y aplicándolos me di cuenta que son una herramienta muy poderosa y relativamente poco explorada en sistemas productivos. En el camino me encontré con las personas que colaboran en MexFlux que para mí han sido una gran inspiración y motivación.

**¿Qué descubrimiento de tu(s) estudio(s)/proyecto(s) ha sido más interesante, sorprendente e inesperado? (compartir ligas a papers)**

En esta publicación nos dimos cuenta que los ensambles de modelos de aprendizaje automático son una herramienta poderosa para predecir la Productividad Primaria Bruta en ecosistemas semiáridos, comparado con una regresión múltiple o con productos de sensores remotos (MODIS). https://bg.copernicus.org/articles/18/367/2021/. Ahora estamos trabajando en un manuscrito para evaluar el producto MODIS que combina mediciones de Fluorescencia inducida por el Sol del observatorio OCO-2 a nivel global.

**¿Qué haces por diversión/hobby vida más allá de la academia?**

Salgo a caminar al campo, cocino cuando estoy deprimida, me gusta la expresión facial de las personas cuando sonríen, leo literatura de terror, hago yoga y ejercicio con peso, pero de verdad me divierte el juego del lobo feroz con mi pequeño hijo.
