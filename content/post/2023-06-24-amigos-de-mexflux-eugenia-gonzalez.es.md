---
title: "Amigos de MexFlux: Eugenia González del Castillo Aranda"
date: 2023-06-24T12:00:00-07:00
categories: ["amigosDeMexFlux"]
tags: ["amigosDeMexFlux"]
---

{{< lead >}}
Eugenia es técnica de proyecto en el Instituto de Ciencias de la Atmósfera y Cambio Climático, UNAM. Ha trabajado en el sitio de Chamela en Jalisco.
{{< /lead >}}

![](../../../img/amigos-de-mexflux/eugenia-gonzalez-small.JPG)

**¿Cuáles son las líneas de investigación que persigues en tu grupo de trabajo?**

Coordino la parte técnica de una red de observatorios atmosféricos (www.ruoa.unam.mx) con presencia en varios estados. Monitoreamos la meteorología en 17 sitios en el país y diversos parámetros atmosféricos en algunos de ellos, como calidad del aire, gases de efecto invernadero, depósito húmedo, entre otros. Yo no hago investigación como tal, aunque me involucro en varios proyectos de investigadores que utilizan estos datos generados por la red, particularmente me interesa el área de concentraciones de gases de efecto invernadero. 


**¿Qué proyecto realizas y/o participas actualmente?**

- Participo en dos redes de monitoreo: Red Universitaria de Observatorios Atmosféricos (RUOA) y el Programa de Estaciones Meteorológicas del Bachillerato Universitario (PEMBU). En particular me involucro en mediciones de CO2 y CH4 en sitios urbanos (Juriquilla y Ciudad Universitaria en la CDMX) y sitios ubicados en ANP (Altzomoni, EdoMex y Chamela, Jalisco)
- Uso de microsensores de bajo y mediano costo para el monitoreo de concentraciones de CO2 en la CDMX (www.epr.atmosfera.unam.mx/Merci-CO2/)
- Mediciones de flujos respiratorios en suelo en el Bosque de Tlalpan
- Participo en los preparativos para el nuevo Observatorio Mexicano del Clima y la Composición Atmosférica (OMECCA) en Calakmul, Campeche, que incluirá una torre de covarianza turbulenta en un sitio de selva subperennifolia. 


**Cuéntanos brevemente cómo o cuándo empezaste a trabajar en el estudio de flujos ecosistémicos.**

El primer proyecto en que me involucré en mediciones de flujos con la técnica de covarianza turbulenta fue en un bosque de álamos en la planicie inundable del río Cosumnes, en California. La teoría detrás de las mediciones, el trabajo de campo con sensores y la complejidad del intercambio de gases y energía en ese sistema me pareció fascinante.  


**¿Qué trabajo o proyecto te llevó a pensar en flujos?**

Para mi tesis de licenciatura medimos flujos de CO2 y H2O a nivel de tallo y hoja, y eso me llevó a preguntarme cómo sería posible escalar este tipo de monitoreo al nivel de paisaje. Aprendí que una de las técnicas para realizar esto era la de covarianza turbulenta, y busqué un posgrado que me permitiera aprender a este respecto. Durante mi trabajo de campo de los estudios de doctorado, tuve la oportunidad de instalar y estar a cargo de una torre de mediciones de intercambio de gases y energía en un bosque tropical caducifolio durante unos 8 años, lo cual era relativamente novedoso en aquél entonces. Entender los procesos de intercambio turbulento, de advección, de cómo interactúa la biota con la atmósfera fue un aprendizaje intenso, emocionante y revelador. 


**¿Qué descubrimiento de tu(s) estudio(s)/proyecto(s) ha sido más interesante, sorprendente e inesperado? (compartir ligas a papers)**

- No fue inesperado, pero la intensidad de los primeros flujos de gases en respuesta a las lluvias en un sistema altamente dependiente de la disponibilidad de agua como el bosque tropical caducifolio de la costa de Jalisco fue algo muy interesante de atestiguar; el bosque adquiere una fisonomía completamente distinta en un periodo de unos 10 días tras el inicio de las lluvias. También la rapidez con la que la vegetación modula su respuesta tanto a precipitaciones en la época seca como a sequías intra-estivales fue sorprendente para mí.
- Descubrir que los modelos de respiración que heredamos de sitios templados pueden ser muy inadecuados para describir la respuesta de sistemas tropicales secos.


**¿Qué haces por diversión/hobby vida más allá de la academia?**

Me gusta leer, cocinar, armar rompecabezas, y disfrutar largas caminatas con Yoko, mi perra viejita y fiel. 
