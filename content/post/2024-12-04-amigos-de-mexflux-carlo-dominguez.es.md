---
title: "Amigos de MexFlux: Carlo Alberto Domínguez Eusebio"
date: 2024-12-04T22:00:00-07:00
categories: ["amigosDeMexFlux"]
tags: ["amigosDeMexFlux"]
---

{{< lead >}}
Carlo es investigado postdoctoral en el Instituto de Ecología (INECOL) trabajando actualmente en estudios de síntesis multisitio y próximamente estará instrumentando un sitio de cafetal.
{{< /lead >}}

![](../../../img/amigos-de-mexflux/carlo-dominguez-small.jpg)


**¿Cuáles son las líneas de investigación que persigues en tu grupo de trabajo?**

Estimación de la productividad primaria gruesa en ecosistemas de latitudes tropicales.


**¿Qué proyecto realizas y/o participas actualmente?**

Intercambio neto de carbono biósfera-atmósfera y revisión de factores de emisión de ecosistemas mexicanos.


**¿Qué te apasiona de tu proyecto?**

El uso de instrumental, modelos y sensores remotos para estimar la productividad primaria


**Cuéntanos brevemente cómo o cuándo empezaste a trabajar en el estudio de flujos ecosistémicos.**

Al final de la licenciatura e inicio del doctorado tuve la oportunidad de operar los instrumentos de eddy covariance para la medición del balance energético superficial sobre un bosque de niebla.


**¿Qué trabajo o proyecto te llevó a pensar en flujos?**

Desde la licenciatura me interesaba conocer más de como la atmósfera interactúa con la superficie, incluyendo la vegetación y los seres vivos.


**¿Qué artículo, ponencia o platica te inspiró a trabajar en lo que haces?**

El libro "The urban energy balance" de Oke


**¿Qué descubrimiento de tu(s) estudio(s)/proyecto(s) ha sido más interesante, sorprendente e inesperado? (compartir ligas a papers)**
La urbanización del bosque de niebla modifica la distribución de la energía, favoreciendo a un calentamiento más rápido del aire y aumentando la tasa de evapotranspiración. https://doi.org/10.1016/j.ufug.2019.03.018


**¿Qué haces por diversión/hobby más allá de la academia?**

Me gusta la tecnología y los videojuegos
