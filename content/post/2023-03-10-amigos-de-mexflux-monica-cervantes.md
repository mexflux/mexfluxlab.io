---
title: "Amigos de MexFlux: Mónica Cervantes Jiménez"
date: 2023-03-10T18:00:00-07:00
categories: ["amigosDeMexFlux"]
tags: ["amigosDeMexFlux"]
---

{{< lead >}}
Monica is a research professor category III at the Facultad de Ciencias Naturales from Universidad Autónoma de Querétaro. 
{{< /lead >}}

![](../../img/amigos-de-mexflux/monica-cervantes-small.jpg)

**Which are the lines of research of your working group?**

Use of remote sensing to model carbon fluxes in agroecosystems.

**Which project are you currently working on and/or participating?**

I am exploring the relationship between Solar Induced Fluorescence (SIF) measured with remote sensing and Gross Primary Productivity at the ecosystem level in the Queretaro semi-desert. I am very interested in the behavior of vegetation phenology and its influence on carbon and water sequestration. Another aspect that interests me is the application of machine learning models and remote sensing in the modelling of agricultural systems, especially with an optimization approach in the use of natural resources to propose sustainability strategies.

**Briefly, tell us how or when did you started working on ecosystem fluxes.**

I am passionate about the ecology of agroecosystems, and as I became more deeply involved in carbon flux research, I realized that many biophysical and biotic processes that can be explored with remote sensing are unknown. Understanding the functioning of ecosystems changed my perspective on the decisions that must be made in production units to reduce environmental impact.

**What study or project lead you to think about fluxes?**

We had carbon flux data from an Eddy covariance tower, and we started working on it, at first, we read some publications, but we didn't understand conventional analysis methods. As we continue to explore the data, we found the machine learning models and applying them I realized that they are a very powerful and relatively little explored tool in production systems. Along the way I met the people who collaborated with MexFlux who greatly inspired and motivated me.

**Which is the most interesting, surprising or unexpected discovery of your study/studies or project(s) (share links to papers)?**

In this publication, we realized that machine learning model ensembles are a powerful tool for predicting Gross Primary Productivity in semi-arid ecosystems, compared to multiple regression or remote sensing (MODIS) products. https://bg.copernicus.org/articles/18/367/2021/. 
We are now working on a manuscript to evaluate the MODIS product that combines the solar-induced fluorescence of the OCO-2 observatory at a global level.

**What do you do for fun or hobby beyond academy?**

I go for walks in the country, I cook when I'm down, I like the facial expressions on people's faces when they smile, I read horror literature, I do yoga and weight training, but I really enjoy playing the game of big bad wolf with my little son.
