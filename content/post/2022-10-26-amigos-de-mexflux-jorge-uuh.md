---
title: "Amigos de MexFlux: Jorge Maximiliano Uuh Sonda"
date: 2022-10-26T13:00:00-07:00
categories: ["amigosDeMexFlux"]
tags: ["amigosDeMexFlux"]
---

{{< lead >}}
Jorge is a CONACYT postdoctoral researcher at Instituto Tecnológico de Sonora (ITSON). He has worked in the sites El Palmar, Yucatán (TDF); Ejido Sisal, Yucatán (Mangrove).
{{< /lead >}}

![](../../img/amigos-de-mexflux/jorge-uuh-small.jpg)

**Which are the research lines of your working group?**

Ecohydrology of Tropical Systems; Vortex Covariance; Remote Perception.

**Which project are you currently working on and/or participating?**

In my Postdoctoral project: Monitoring the exchange of water, carbon and energy flux between the mangrove and the atmosphere as a strategy for resilience and adaptation to climate change in Ejido Sisal, Yucatán.

**Briefly tell us how or when you started working on ecosystem fluxes.**

It was during my PhD, and as I always say, I arrived in a random and unconventional way. In my master's degree, I was working on a completely different thing, Astrophysics. Various reasons, personal and professional, made me question whether to continue along that line or return to my origins as an Engineer with field work and handling of instruments. In the search for options, I found an excellent Institute of Engineering, very close to my hometown and whose lines of research allowed me to work on another subject I was passionate about, Earth Sciences. This is how I connected with lines of research related to Ecohydrology and studying the Dynamics of Ecosystemic Flux through the Eddy Covariance technique.

**What study or project lead you to think about fluxes?**

Just my doctoral project. This consisted of installing, implementing and managing a Vortex Covariance system to study the Temporary Dynamics of Ecosystemic Flux of the Low Deciduous Forest in the Northwest of the Yucatan Penins, whose balance and extension have been threatened in recent years, for both urban and commercial development.

**Which is the most interesting, surprising or unexpected discovery of your study/studies or project(s) (share links to papers)?**

The novelty of the studies in which I have been involved is that we have begun to understand and describe, from the point of view of these water and C flux dynamics, the behaviour of the different ecosystems of the Yucatan Peninsula. Very little was known about the eco-hydrological interactions of these biomes, their intra- and inter-annual occurrence, their relationship with climate, and their temporal dependence on groundwater.

https://onlinelibrary.wiley.com/doi/abs/10.1002/hyp.13230
https://agupubs.onlinelibrary.wiley.com/doi/abs/10.1029/2019JG005629

**What do you do for fun or a hobby beyond the academy?**

I really enjoy watching TV series, especially comedy, to whatch movies or going out to dinner with my fiancée Aurea, when there is an opportunity to drink a good craft beer with friends or family, as well as play board games. When it comes to sports, I like football and squash. Finally, I love to travel and discover new places.
