---
title: "Amigos de MexFlux: Aylin Cristina Barreras Apodaca"
date: 2022-10-17T19:00:00-07:00
categories: ["amigosDeMexFlux"]
tags: ["amigosDeMexFlux"]
---

{{< lead >}}
Aylin is a consultant in the Cooperación Técnica Forestal México - Estados Unidos (Forestry Technical Cooperation Mexico - United States)
{{< /lead >}}

![](../../img/amigos-de-mexflux/aylin-barreras-small.jpg)

**Which are the lines of research of your working group?**

To quantify the carbon budget of terrestrial ecosystems (i.e., forests, crops, etc.) and digital mapping of carbon in soils and vegetation through machine learning.

**Which project are you currently working on and/or participating?**

Digital mapping of traits (i.e., biomass, stored carbon, tree height) and ecological function of Mexican forests.

**Briefly, tell us how or when did you started working on ecosystem fluxes.**

I had the opportunity to work with data on CO2 fluxes of mangroves when I was on the last semesters of my bachelor. That work was presented in the Mexican Geophysical Union. After that, I worked with CO2 fluxes at a seagrass prairie and coastal lagoon.

**What study or project lead you to think about fluxes?**

The high potential of mangrove ecosystems for mitigating the atmospheric CO2.

**Which is the most interesting, surprising or unexpected discovery of your study/studies or project(s) (share links to papers)?**

High rates of air-water CO2 exchanges above seagrass prairie.
(https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2020GB006848)

**What do you do for fun or hobby beyond academy?**

Take my little dog Lola for a walk, listen to podcasts of feminist women, and making infographics for kids about environmental topics.
