---
title: "Amigos de MexFlux: Susana Alvarado Barrientos"
date: 2023-06-24T12:30:00-07:00
categories: ["amigosDeMexFlux"]
tags: ["amigosDeMexFlux"]
---

{{< lead >}}
Susana is an Associate Professor at Instituto de Ecología A.C. (INECOL) in Xalapa, Veracruz. She has worked at the sites MX-PMm and MX-Ord.
{{< /lead >}}

![](../../img/amigos-de-mexflux/susana-alvarado-small.jpg)


**Which are the lines of research of your working group?**

My research and teaching work is part of ecohydrology and ecosystem-atmosphere interactions, that is, the exchange of water, carbon and energy in the critical zone of the planet. I am interested in knowing the basic functioning of ecosystems to understand the biophysical effects of anthropogenic modifications, such as changes in land use and the development of ecological restoration of degraded ecosystems. I focus on studying humid tropical and mountain environments.


**Which project are you currently working on and participating in?**

We are currently studying soil-atmosphere carbon fluxes in ecosystems that are being restored (particularly mangroves, montane cloud forests, and their riparian zones). We also continue to explore the ecohydrological functioning of shade-grown coffee plantations (particularly the water consumption of different varieties and structural arrangements).


**Briefly tell us how or when you started working on ecosystem fluxes.**

I was very interested in it from my first master's classes. I found it wonderful and almost magical that we can measure something invisible and simultaneously so fundamental to life as the exchange of water and carbon between biota and the atmosphere. Then I could climb a flow tower 40 m high in the Amazon jungle. There, I wanted to participate in this type of research and contribute to the world network of people dedicated to knowing the basic functioning of ecosystems. I completed my doctorate studying the transpiration of trees planted with sap flow sensors. I applied the eddy covariance technique during my postdoctoral studies, contrasting the energy balance of three land uses in the cloud forest area. I had only worked with water and energy flows, and it was when I was granted my first infrastructure project as a young researcher that allows me to apply the technique to find out how carbon flows change in a mangrove when it is not flooded.


**What study or project lead you to think about fluxes?**

More than work or project, I came to think about flows because of my love of observing clouds and fog in the cloud forest and my interest in better understanding the forest-water link. This interest led me to pursue a master's degree in ecohydrology in Amsterdam because they had projects on these topics, and that's where everything started to "flow" :)


**Which is the most interesting, surprising or unexpected discovery of your study/studies or project(s) (share links to papers)?**

From my work with the flow tower in an inland mangrove swamp, it was interesting to observe that during the most extended period when the mangrove swamp was not flooded, it acted as a carbon source to the atmosphere. The most surprising thing was that during this period, carbon assimilation decreased, but evapotranspiration did not; we observed a decoupling of carbon and water exchange, indicating extreme conditions for the mangrove ecosystem.
https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2020JG005811


**What do you do for fun or a hobby beyond the academy?**
For fun, I go for a walk in parks and around Xalapa. I also really like traveling and going to museums and reading. In addition, I like to spend fun time with the occurrences of my son Lucas.

