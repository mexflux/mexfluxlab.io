---
description: Página inicial del sitio web de MexFlux
---
# ¡Somos MexFlux! [{{< icon "facebook" >}}](https://www.facebook.com/MexFlux)[{{< icon "twitter" >}}](https://twitter.com/mexflux)[{{< icon "email" >}}](mailto:mexflux@gmail.com)

{{< lead >}}
El consorcio mexicano de monitoreo de flujos de gases de efecto invernadero entre los ecosistemas y la atmósfera, y ahora tambien Laboratorio Nacional Conahcyt - MexFlux
{{< /lead >}}

Los ecosistemas mexicanos son únicos y en MexFlux tratamos de entender su funcionamiento. En nuestros sitios de estudio medimos el intercambio de energía, agua, dióxido de carbono y otros gases traza y de efecto invernadero entre los ecosistemas y la atmósfera. Estas mediciones nos permiten evaluar la capacidad de los ecosistemas mexicanos para mitigar el cambio climático, y cómo responden los ecosistemas a cambios ambientales, lo cual es altamente relevante para formular políticas públicas orientadas a un manejo sustentable de los recursos terrestres e hídricos.

![](../img/churi_natural_small_reduced.jpg "Observatorio La Colorada matorral (foto por Cesar Hinojo Hinojo)") 

