---
description: MexFlux publications
title: Publications
showTableOfContents: true
---
{{< lead >}}
**Check our new [Special Issue in JGR: Biogeosciences](https://agupubs.onlinelibrary.wiley.com/doi/toc/10.1002/(ISSN)2169-8961.MEXFLUX) and our foundational papers from [Vargas and Yepez 2011](https://doi.org/10.1029/2011EO370004) and [Vargas et al 2013](https://doi.org/10.1016/S0187-6236(13)71079-8)**
{{< /lead >}}


## 2022

* Jongen, H. J., Steeneveld, G. J., Beringer, J., Christen, A., Chrysoulakis, N., Fortuniak, … Teuling, A. J. (2022). Urban Water Storage Capacity Inferred From Observed Evapotranspiration Recession. Geophysical Research Letters, 49(3). https://doi.org/10.1029/2021GL096069


## 2021

* Villarreal, S., & Vargas, R. (2021). Representativeness of FLUXNET Sites Across Latin America. Journal of Geophysical Research: Biogeosciences, 126(3). https://doi.org/10.1029/2020JG006090
* Pérez‐Ruiz, E. R., Vivoni, E. R., Yépez, E. A., Rodríguez, J. C., Gochis, D. J., Robles‐Morua, A., … Adams, D. K. (2021). Landscape controls on water‐energy‐carbon fluxes across different ecosystems during the North American monsoon. Journal of Geophysical Research: Biogeosciences, 126(5). https://doi.org/10.1029/2020JG005809
* Holwerda, F., Guerrero-Medina, O., & Meesters, A. G. C. A. (2021). Evaluating surface renewal models for estimating sensible heat flux above and within a coffee agroforestry system. Agricultural and Forest Meteorology, 308–309. https://doi.org/10.1016/j.agrformet.2021.108598
* Hidalgo-Sánchez, M., Ángeles-Pérez, G., Yépez, E. A., Plascencia-Escalante, F. O., Delgado-Balbuena, J., & González-Martínez, T. M. (2021). Evapotranspiración e intercambio de energía en un bosque templado de México. Tecnología y Ciencias Del Agua, 12(2). https://doi.org/10.24850/j-tyca-2021-02-11
* Granados‐Martínez, K. P., Yépez, E. A., Sánchez‐Mejía, Z. M., Gutiérrez‐Jurado, H. A., & Méndez‐Barroso, L. A. (2021). Environmental controls on the temporal evolution of energy and CO2 fluxes on an arid mangrove of Northwestern Mexico. Journal of Geophysical Research: Biogeosciences, 126(7). https://doi.org/10.1029/2020JG005932
* Salvador Castillo, J. M., Bolaños González, M. A., Rodríguez, J. C., Palacios Vélez, E., Palacios Sánchez, L. A., Watts, C., … Er Raki, S. (2021). Estimación de la evapotranspiración de un viñedo de uva de mesa (Vitis vinífera) con imágenes satelitales Sentinel-2. Agrociencia, 55(5). https://doi.org/10.47163/agrociencia.v55i5.2514
* Alvarado‐Barrientos, M. S., López‐Adame, H., Lazcano‐Hernández, H. E., Arellano‐Verdejo, J., & Hernández‐Arana, H. A. (2021). Ecosystem‐atmosphere exchange of CO2, water, and energy in a basin mangrove of the Northeastern coast of the Yucatan Peninsula. Journal of Geophysical Research: Biogeosciences, 126(2). https://doi.org/10.1029/2020JG005811
* Cueva, A., Bullock, S. H., Méndez‐Alonzo, R., López‐Reyes, E., & Vargas, R. (2021). Foliage senescence as a key parameter for modeling gross primary productivity in a Mediterranean shrubland. Journal of Geophysical Research: Biogeosciences, 126(1). https://doi.org/10.1029/2020JG005839
* Guevara-Escobar, A., González-Sosa, E., Cervantes-Jiménez, M., Suzán-Azpiri, H., Queijeiro-Bolaños, M. E., Carrillo-Ángeles, I., & Cambrón-Sandoval, V. H. (2021). Machine learning estimates of eddy covariance carbon flux in a scrub in the Mexican highland. Biogeosciences, 18(2). https://doi.org/10.5194/bg-18-367-2021


## 2020

* Vargas Terminel, M. L., Yépez, E. A., Tarin, T., Robles Zazueta, C. A., Garatuza Payán, J., Rodríguez, J. C., … Vivoni, E. R. (2020). Contribución del estrato arbustivo a los flujos de agua y CO2 de un matorral subtropical en el Noroeste de México. Tecnología y Ciencias Del Agua, 11(5). https://doi.org/10.24850/j-tyca-2020-05-04
* Tarin, T., Yepez, E. A., Garatuza-Payan, J., Rodriguez, J. C., Méndez-Barroso, L. A., Watts, C. J., & Vivoni, E. R. (2020). Evapotranspiration flux partitioning at a multi-species shrubland with stable isotopes of soil, plant, and atmosphere water pools. Atmósfera. https://doi.org/10.20937/ATM.52710
* Rojas‐Robles, N. E., Garatuza‐Payán, J., Álvarez‐Yépiz, J. C., Sánchez‐Mejía, Z. M., Vargas, R., & Yépez, E. A. (2020). Environmental controls on carbon and water fluxes in an old‐growth tropical dry forest. Journal of Geophysical Research: Biogeosciences, 125(8). https://doi.org/10.1029/2020JG005666


## 2019

* Villarreal, S., Guevara, M., Alcaraz‐Segura, D., & Vargas, R. (2019). Optimizing an environmental observatory network design using publicly available data. Journal of Geophysical Research: Biogeosciences, 124(7). https://doi.org/10.1029/2018JG004714
* Holwerda, F., & Meesters, A. G. C. A. (2019). Soil evaporation in a shaded coffee plantation derived from eddy covariance measurements. Journal of Geophysical Research: Biogeosciences, 124(6). https://doi.org/10.1029/2018JG004911
* Hinojo-Hinojo, C., Castellanos, A. E., Huxman, T., Rodriguez, J. C., Vargas, R., Romo-León, J. R., & Biederman, J. A. (2019). Native shrubland and managed buffelgrass savanna in drylands: Implications for ecosystem carbon and water fluxes. Agricultural and Forest Meteorology, 268. https://doi.org/10.1016/j.agrformet.2019.01.030
* Delgado‐Balbuena, J., Arredondo, J. T., Loescher, H. W., Pineda‐Martínez, L. F., Carbajal, J. N., & Vargas, R. (2019). Seasonal precipitation legacy effects determine the carbon balance of a semiarid grassland. Journal of Geophysical Research: Biogeosciences, 124(4). https://doi.org/10.1029/2018JG004799
* Aguirre-Gutiérrez, C. A., Holwerda, F., Goldsmith, G. R., Delgado, J., Yepez, E., Carbajal, N., … Arredondo, J. T. (2019). The importance of dew in the water balance of a continental semiarid grassland. Journal of Arid Environments, 168. https://doi.org/10.1016/j.jaridenv.2019.05.003


## 2018

* Verduzco, V. S., Vivoni, E. R., Yépez, E. A., Rodríguez, J. C., Watts, C. J., Tarin, T., … Ivanov, V. Y. (2018). Climate change impacts on net ecosystem productivity in a subtropical shrubland of Northwestern México. Journal of Geophysical Research: Biogeosciences, 123(2). https://doi.org/10.1002/2017JG004361
* Uuh-Sonda, J. M., Gutiérrez-Jurado, H. A., Figueroa-Espinoza, B., & Méndez-Barroso, L. A. (2018). On the ecohydrology of the Yucatan Peninsula: Evapotranspiration and carbon intake dynamics across an eco-climatic gradient. Hydrological Processes, 32(18). https://doi.org/10.1002/hyp.13230
* Hinojo-Hinojo, C., Castellanos, A. E., Llano-Sotelo, J., Peñuelas, J., Vargas, R., & Romo-Leon, J. R. (2018). High Vcmax, Jmax and photosynthetic rates of Sonoran Desert species: Using nitrogen and specific leaf area traits as predictors in biochemical models. Journal of Arid Environments, 156. https://doi.org/10.1016/j.jaridenv.2018.04.006
* Gonzalez del Castillo, E., Sanchez-Azofeifa, A., Paw U, K. T., Gamon, J. A., & Quesada, M. (2018). Integrating proximal broad-band vegetation indices and carbon fluxes to model gross primary productivity in a tropical dry forest. Environmental Research Letters, 13(6). https://doi.org/10.1088/1748-9326/aac3f0
* González del Castillo, E., Paw U, K. T., & Sánchez-Azofeifa, A. (2018). Turbulence scales for eddy covariance quality control over a tropical dry forest in complex terrain. Agricultural and Forest Meteorology, 249. https://doi.org/10.1016/j.agrformet.2017.11.014
* Garatuza-Payan, J., Argentel-Martinez, L., Yepez, E. A., & Arredondo, T. (2018). Initial response of phenology and yield components of wheat (Triticum durum L., CIRNO C2008) under experimental warming field conditions in the Yaqui Valley. PeerJ, 6. https://doi.org/10.7717/peerj.5064


## 2017

* Cueva, A., Bullock, S. H., López-Reyes, E., & Vargas, R. (2017). Potential bias of daily soil CO2 efflux estimates due to sampling time. Scientific Reports, 7(1). https://doi.org/10.1038/s41598-017-11849-y
* Castro, A., Martínez-Osuna, J. F., Michel, R., Escoto-Rodríguez, M., Bullock, S. H., Cueva, A., … Vargas, R. (2017). A low-cost modular data-acquisition system for monitoring biometeorological variables. Computers and Electronics in Agriculture, 141. https://doi.org/10.1016/j.compag.2017.08.010
* Biederman, J. A., Scott, R. L., Bell, T. W., Bowling, D. R., Dore, S., Garatuza‐Payan, J., … Goulden, M. L. (2017). CO2 exchange and evapotranspiration across dryland ecosystems of southwestern North America. Global Change Biology, 23(10). https://doi.org/10.1111/gcb.13686


## 2016
* Velasco, E., Roth, M., Norford, L., & Molina, L. T. (2016). Does urban vegetation enhance carbon sequestration? Landscape and Urban Planning, 148, 99–107. https://doi.org/10.1016/j.landurbplan.2015.12.003
* Villarreal, S., Vargas, R., Yepez, E. A., Acosta, J. S., Castro, A., Escoto‐Rodriguez, M., … Watts, C. J. (2016). Contrasting precipitation seasonality influences evapotranspiration dynamics in water‐limited shrublands. Journal of Geophysical Research: Biogeosciences, 121(2). https://doi.org/10.1002/2015JG003169
* Holwerda, F., Alvarado-Barrientos, M. S., & González-Martínez, T. M. (2016). Surface energy exchange in a tropical montane cloud forest environment: Flux partitioning, and seasonal and land cover-related variations. Agricultural and Forest Meteorology, 228–229. https://doi.org/10.1016/j.agrformet.2016.06.011
* Biederman, J. A., Scott, R. L., Goulden, M. L., Vargas, R., Litvak, M. E., Kolb, T. E., … Burns, S. P. (2016). Terrestrial carbon balance in a drier world: the effects of water availability in southwestern North America. Global Change Biology, 22(5). https://doi.org/10.1111/gcb.13222
* Hinojo-Hinojo, C., Castellanos, A. E., Rodriguez, J. C., Delgado-Balbuena, J., Romo-León, J. R., Celaya-Michel, H., & Huxman, T. E. (2016). Carbon and water fluxes in an exotic buffelgrass savanna. Rangeland Ecology & Management, 69(5), 334–341. https://doi.org/10.1016/j.rama.2016.04.002


## 2011-2015

* Verduzco, V. S., Garatuza-Payán, J., Yépez, E. A., Watts, C. J., Rodríguez, J. C., Robles-Morua, A., & Vivoni, E. R. (2015). Variations of net ecosystem production due to seasonal precipitation differences in a tropical dry forest of northwest Mexico. Journal of Geophysical Research: Biogeosciences, 120(10). https://doi.org/10.1002/2015JG003119
* Reimer, J. J., Vargas, R., Rivas, D., Gaxiola-Castro, G., Hernandez-Ayon, J. M., & Lara-Lara, R. (2015). Sea surface temperature influence on terrestrial gross primary production along the Southern California Current. PLOS ONE, 10(4). https://doi.org/10.1371/journal.pone.0125177
* Cueva, A., Bahn, M., Litvak, M., Pumpanen, J., & Vargas, R. (2015). A multisite analysis of temporal random errors in soil CO2 efflux. Journal of Geophysical Research: Biogeosciences, 120(4). https://doi.org/10.1002/2014JG002690
* Velasco, E., Perrusquia, R., Jiménez, E., Hernández, F., Camacho, P., Rodríguez, S., … Molina, L. T. (2014). Sources and sinks of carbon dioxide in a neighborhood of Mexico City. Atmospheric Environment, 97. https://doi.org/10.1016/j.atmosenv.2014.08.018
* Méndez-Barroso, L. A., Vivoni, E. R., Robles-Morua, A., Mascaro, G., Yépez, E. A., Rodríguez, J. C., … Saíz-Hernández, J. A. (2014). A modeling approach reveals differences in evapotranspiration and its partitioning in two semiarid ecosystems in Northwest Mexico. Water Resources Research, 50(4). https://doi.org/10.1002/2013WR014838
* Leon, E., Vargas, R., Bullock, S., Lopez, E., Panosso, A. R., & La Scala, N. (2014). Hot spots, hot moments, and spatio-temporal controls on soil CO2 efflux in a water-limited ecosystem. Soil Biology and Biochemistry, 77. https://doi.org/10.1016/j.soilbio.2014.05.029
* Chirouze, J., Boulet, G., Jarlan, L., Fieuzal, R., Rodriguez, J. C., Ezzahar, J., … Chehbouni, G. (2014). Intercomparison of four remote-sensing-based energy balance methods to retrieve surface evapotranspiration and water stress of irrigated fields in semi-arid climate. Hydrology and Earth System Sciences, 18(3). https://doi.org/10.5194/hess-18-1165-2014
* Vargas, R., Yépez, E. A., Andrade, J. L., Ángeles, G., Arredondo, T., Castellanos, A. E., … Watts, C. (2013). Progress and opportunities for monitoring greenhouse gases fluxes in Mexican ecosystems: the MexFlux network. Atmósfera, 26(3). https://doi.org/10.1016/S0187-6236(13)71079-8
* Holwerda, F., Bruijnzeel, L. A., Barradas, V. L., & Cervantes, J. (2013). The water and energy exchange of a shaded coffee plantation in the lower montane cloud forest zone of central Veracruz, Mexico. Agricultural and Forest Meteorology, 173. https://doi.org/10.1016/j.agrformet.2012.12.015
* Vargas, R. (2012). How a hurricane disturbance influences extreme CO2 fluxes and variance in a tropical forest. Environmental Research Letters, 7(3). https://doi.org/10.1088/1748-9326/7/3/035704
* Vargas, R., & Yépez, E. A. (2011). Toward a Mexican eddy covariance network for carbon cycle science. Eos, Transactions American Geophysical Union, 92(37). https://doi.org/10.1029/2011EO370004
* Velasco, E., Pressley, S., Grivicke, R., Allwine, E., Molina, L. T., & Lamb, B. (2011). Energy balance in urban Mexico City: observation and parameterization during the MILAGRO/MCMA-2006 field campaign. Theoretical and Applied Climatology, 103(3–4). https://doi.org/10.1007/s00704-010-0314-7


## 1998-2010
* Vivoni, E. R., Watts, C. J., Rodríguez, J. C., Garatuza-Payan, J., Méndez-Barroso, L. A., & Saiz-Hernández, J. A. (2010). Improved land–atmosphere relations through distributed footprint sampling in a subtropical scrubland during the North American monsoon. Journal of Arid Environments, 74(5). https://doi.org/10.1016/j.jaridenv.2009.09.031
* Velasco, E., & Roth, M. (2010). Cities as net sources of CO2: Review of atmospheric CO2 exchange in urban environments measured by eddy covariance technique. Geography Compass, 4(9). https://doi.org/10.1111/j.1749-8198.2010.00384.x
* Rodríguez, J. C., Grageda, J., Watts, C. J., Garatuza-Payan, J., Castellanos-Villegas, A., Rodríguez-Casas, J., … Olavarrieta, V. (2010). Water use by perennial crops in the lower Sonora watershed. Journal of Arid Environments, 74(5). https://doi.org/10.1016/j.jaridenv.2009.11.008
* Perez-Ruiz, E. R., Garatuza-Payan, J., Watts, C. J., Rodriguez, J. C., Yepez, E. A., & Scott, R. L. (2010). Carbon dioxide and water vapour exchange in a tropical dry forest as influenced by the North American Monsoon System (NAMS). Journal of Arid Environments, 74(5). https://doi.org/10.1016/j.jaridenv.2009.09.029
* Lizárraga-Celaya, C., Watts, C. J., Rodríguez, J. C., Garatuza-Payán, J., Scott, R. L., & Sáiz-Hernández, J. (2010). Spatio-temporal variations in surface characteristics over the North American Monsoon region. Journal of Arid Environments, 74(5). https://doi.org/10.1016/j.jaridenv.2009.09.027
* Velasco, E., Pressley, S., Grivicke, R., Allwine, E., Coons, T., Foster, W., … Lamb, B. (2009). Eddy covariance flux measurements of pollutant gases in urban Mexico City. Atmospheric Chemistry and Physics, 9(19). https://doi.org/10.5194/acp-9-7325-2009
* Vivoni, E. R., Moreno, H. A., Mascaro, G., Rodriguez, J. C., Watts, C. J., Garatuza-Payan, J., & Scott, R. L. (2008). Observed relation between evapotranspiration and soil moisture in the North American monsoon region. Geophysical Research Letters, 35(22). https://doi.org/10.1029/2008GL036001
* Abdelghani, C., Hoedjes, J. C. B., Rodriquez, J.-C., Watts, C. J., Garatuza, J., Jacob, F., & Kerr, Y. H. (2008). Using remotely sensed data to estimate area-averaged daily surface fluxes over a semi-arid mixed agricultural land. Agricultural and Forest Meteorology, 148(3). https://doi.org/10.1016/j.agrformet.2007.09.014
* Watts, C. J., Scott, R. L., Garatuza-Payan, J., Rodriguez, J. C., Prueger, J. H., Kustas, W. P., & Douglas, M. (2007). Changes in Vegetation Condition and Surface Fluxes during NAME 2004. Journal of Climate, 20(9). https://doi.org/10.1175/JCLI4088.1
* Velasco, E., Lamb, B., Pressley, S., Allwine, E., Westberg, H., Jobson, B. T., … & Molina, M. (2005). Flux measurements of volatile organic compounds from an urban landscape. Geophysical Research Letters, 32(20), L20802. https://doi.org/10.1029/2005GL023356
* Velasco, E., Pressley, S., Allwine, E., Westberg, H., & Lamb, B. (2005). Measurements of CO fluxes from the Mexico City urban landscape. Atmospheric Environment, 39(38). https://doi.org/10.1016/j.atmosenv.2005.08.038
* Chehbouni, A., Watts, C., Kerr, Y. H., Dedieu, G., Rodriguez, J. C., Santiago, S., … Goodrich, D. C. (2000). Methods to aggregate turbulent fluxes over heterogeneous surfaces: application to SALSA data set in Mexico. Agricultural and Forest Meteorology, 105(1–3), 133–144. https://doi.org/10.1016/S0168-1923(00)00185-4
* Goodrich, D. C., Chehbouni, A., Goff, B., MacNish, B., Maddock, T., Moran, … Yucel, J. I. (2000). Preface paper to the Semi-Arid Land-Surface-Atmosphere (SALSA) Program special issue. Agricultural and Forest Meteorology, 105(1–3), 3–20. https://doi.org/10.1016/S0168-1923(00)00178-7
* Garatuza-Payan, J., Shuttleworth, W. J., Encinas, D., McNeil, D. D., Stewart, J. B., DeBruin, H., & Watts, C. (1998). Measurement and modelling evaporation for irrigated crops in north-west Mexico. Hydrological Processes, 12(9). https://doi.org/10.1002/(SICI)1099-1085(199807)12:9<1397::AID-HYP644>3.0.CO;2-E
