---
description: Aviso de privacidad
title: Aviso de privacidad integral
---

El consorcio para el monitoreo del intercambio de gases de efecto invernadero y energía entre los ecosistemas mexicanos y la atmósfera, mejor conocido como MexFlux, sin domicilio físico y con portal de internet en https://mexflux.gitlab.io, es el responsable del uso y protección de sus datos personales, y al respecto le informamos lo siguiente:

## ¿Para qué fines utilizaremos sus datos personales?

Los datos personales que recabamos de usted los utilizaremos para las siguientes finalidades:

1) En el sitio web, para establecer una persona(s) de contacto responsable de cada uno de los observatorios MexFlux, para propositos de difusion, y como referencia a posibles interesados en colaborar o usar los datos de dichos sitios.
2) En la lista de correo de MexFlux, para mantener comunicacion entre los miembros y personas relacionadas al consorcio MexFlux 

## ¿Qué datos personales utilizaremos para estos fines?

Para llevar a cabo las finalidades descritas en el presente aviso de privacidad, el sitio web utiliza y publica los siguientes datos personales:

- Nombre y direccion de correo electronico del investigador responsable y/o alguna otra persona que funge como contacto para cada uno de los observatorios MexFlux
- Caracteristicas del observatorio del que es responsable, tales como (pero no limitadas a) nombre, localizacion geografica, años con datos, tipo de vegetatcion, instrumentacion y otras mediciones realizadas en el sitio.

Por su parte, la lista de correo de MexFlux utiliza los siguientes datos personales, mismos que nos son compartidos con ninguna autoridad, empresa, organización o persona distintas a nosotros:

- Direccion de correo electronico
- Nombre

## ¿Cómo puede acceder, rectificar o cancelar sus datos personales, u oponerse a su uso o ejercer la revocación de consentimiento?

Usted tiene derecho a conocer qué datos personales tenemos de usted, para qué los utilizamos y las condiciones del uso que les damos (Acceso). Asimismo, es su derecho solicitar la corrección de su información personal en caso de que esté desactualizada, sea inexacta o incompleta (Rectificación); que la eliminemos de nuestros registros o bases de datos cuando considere que la misma no está siendo utilizada adecuadamente (Cancelación); así como oponerse al uso de sus datos personales para fines específicos (Oposición). Estos derechos se conocen como derechos ARCO.

Para el ejercicio de cualquiera de los derechos ARCO, debe enviar una petición en formato libre vía correo electrónico a hinojo_@hotmail.com y deberá contener:

- Nombre completo del titular.
- Correo electrónico usado en este sitio web o en la lista de correo.
- Asunto «Derechos ARCO y MexFlux»
- Descripción del objeto del escrito, los cuales pueden ser de manera enunciativa más no limitativa los siguientes: Revocación del consentimiento para tratar sus datos personales; y/o Notificación del uso indebido del tratamiento de sus datos personales; y/o Ejercitar sus Derechos ARCO, con una descripción clara y precisa de los datos a Acceder, Rectificar, Cancelar o bien, Oponerse. En caso de Rectificación de datos personales, deberá indicar la modificación exacta. Asimismo, usted deberá considerar que para ciertos fines, la revocación de su consentimiento implicará que no le podamos seguir prestando el servicio que brinda el sitio web (difusion de los observatorios y actividades de la red) o la lista de correos (comunicacion entre interesados en MexFlux).

## ¿En cuántos días le daremos respuesta a su solicitud?
20 días

## ¿Por qué medio le comunicaremos la respuesta a su solicitud?
Al mismo correo electrónico de donde se envío la petición.

## El uso de tecnologías de rastreo en nuestro portal de internet

Al conocimiento del desarrollador del sitio, nuestro sitio web NO utiliza cookies, web beacons u otras tecnologías, a través de las cuales es posible monitorear su comportamiento como usuario de internet.

## ¿Cómo puede conocer los cambios en este aviso de privacidad?

El presente aviso de privacidad puede sufrir modificaciones, cambios o actualizaciones derivadas de nuevos requerimientos legales; de nuestras propias necesidades; de nuestras prácticas de privacidad; o por otras causas. Nos comprometemos a mantener actualizado este aviso de privacidad sobre los cambios que pueda sufrir y siempre podrá consultar las actualizaciones que existan en el sitio web https://mexflux.gitlab.io/privacy/aviso-privacidad.html

Última actualización de este aviso de privacidad: 28/Marzo/2022
