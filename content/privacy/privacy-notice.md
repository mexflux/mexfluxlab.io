---
description: Privacy notice
title: Privacy notice
---

The network for monitoring the exchange of greenhouse gases, water and energy between Mexican ecosystem and the atmosphere, better known as MexFlux, without a physical address and with web site address at https://mexflux.gitlab.io, is responsible for the use and protection of your personal data, and regarding that matter, we inform you the following:

## Why do we use your personal data?

The personal data that we gather from you is used for the following purposes:

1) At the MexFlux website, in order to establish pa contact person(s) in charge of each one of the MexFlux sites, for outreach, as a reference to possible folks or groups interested in collaborating or use the data of the sites.
2) At the MexFlux mail list, to maintain communication between members and other folks related to MexFlux.

## What data do we use for those purposes?

In order to fulfil the purposes described in this privacy notice, the website uses and publishes the following personal data:

- Name and email address of the principal investigator and/or any other person that serves as a contact person for each of the MexFlux sites.
- Characteristics of the site that such person is in charge, such as (but not limited to) name of the site, geographic location, years with data, vegetation type, instrumentation, and other measurements made at the site.

The MexFlux mail list uses the following personal data, which are not shared with any authority, company, organization, or any person not related to MexFlux:

- Email address
- Name

## How to access, rectify, or cancel its personal data, reject its use, or withdraw consent?

You have the right to know which personal data we gather and use from you, how and why do we use them, and the conditions for the use we make (Access). In addition, it is your right to request a correction for your personal information in case it is not updated, or its not exact or incomplete (Rectification); that we delete from our archives or data bases when you consider that the information is not being properly used (Cancellation); and to refuse the use of your personal data for specific purposes (Opposition).

To exercise any of these rights, you may send a written request in free format via email to hinojo_@hotmail.com, which should contain:

- Your name
- Email address used in the website or mail list
- Subject «Personal data rights»
- Describe the purpose of the request, which may list any (but not limited to) the following: withdrawal of consent to use your personal data; notify improper use and handling of your personal data; excercise you personal data rights (stated above); with a clear and precise description of the data that you want to Access, Rectify, Cancel, or Refuse/Oppose. In case of Rectification of your personal data, you should indicate the exact modification. In addition, you should consider that for certain purposes, the withdrawal of consent may imply that MexFlux will not be able to continue providing the service that the website carries (outreach of the MexFlux sites and the activities of the network) or the mail list (communication among those interested in MexFlux).

## How long would it take us to answer your request?
Within 20 days

## How will we comunicate our answer to your request?
To the same email address from which the request was sent.

## The use of tracking technologies in the MexFlux website

To the best of the knowledge of the web developer, the MexFlux website does NOT use cookies, web beacons or other technologies that could make possible to track your behaviour as internet user.

## How to review changes in this privacy notice?

This privacy notice may be modified or updated, due to law requirements, our own needs, our own privacy policies, or other causes. We will keep this privacy notice updated on any change made to it, and you could always check any updates that happen to it at the web site https://mexflux.gitlab.io/privacy/aviso-privacidad.html

This privacy notice was last updated on March 28th, 2022.
