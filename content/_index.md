---
description: Landing page for the MexFlux website
---

# We are Mexflux! [{{< icon "facebook" >}}](https://www.facebook.com/MexFlux)[{{< icon "twitter" >}}](https://twitter.com/mexflux)[{{< icon "email" >}}](mailto:mexflux@gmail.com)

{{< lead >}}
The Mexican ecosystem-atmosphere fluxes network, and now also Conahcyt National Laboratory - MexFlux
{{< /lead >}} 

Mexican ecosystems are unique and we are getting to understand how they work. We are measuring how Mexican ecosystems exchange energy, water, carbon dioxide, and other trace and greenhouse gases with the atmosphere. These measurements help assess the capacity of Mexican ecosystems to mitigate climate change, are highly relevant for improving land and water management, and to inform policy. 

![](img/churi_natural_small_reduced.jpg "La Colorada shrubland site (picture by Cesar Hinojo Hinojo)")
