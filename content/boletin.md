---
description: MexFlux newsletter
title: MexFlux newsletter
---

{{< lead >}}
Our newsletter is our newest community communication and outreach effort. On each issue, the MexFlux community will announce its recent and future activities, and also is a space where the community can share pictures, field notes, ideas, projects, adds. For now, we will only release the newsletter in Spanish.
{{< /lead >}}

Click on the following links to download the issues:

[Issue 2 (January 2025)](https://drive.google.com/file/d/1-1EdtRgPloPQiQLWxl0HFlOYvHarbyM6/view?usp=sharing)

[Issue 1 (November 2024)](https://drive.google.com/file/d/1zMqg5H2aBlQ_dFq1A_BAMJ9jxGUVdHi2/view?usp=sharing)
