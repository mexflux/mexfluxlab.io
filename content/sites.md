---
description: Location and description of MexFlux sites
title: Sites
---

{{< lead >}}
Explore the sites in this interactive map (active sites in redish color) or in the table below (active sites in bold)
{{< /lead >}}


<!-- Code from https://faq.dailymotion.com/hc/en-us/articles/360022841393-How-to-preserve-the-player-aspect-ratio-on-a-responsive-page -->

<div style="position:relative;padding-bottom:75%;">
 <iframe style="width:100%;height:100%;position:absolute;left:0px;top:0px;"
 frameborder="0" width="100%" height="100%" 
 src="../mexflux_site_map_2024.html">
</iframe>
</div>

| **Site name**                                   | **Site ID** | **years**                 | **PI**                                  | **Email**                                                      | **Veg type**                      | **Data at**                                                  |
|-------------------------------------------------|-------------|---------------------------|-----------------------------------------|----------------------------------------------------------------|-----------------------------------|--------------------------------------------------------------|
| Alamos Acacia                                   | MX-Aca      | 3 (2016-2018)             | Enrico A. Yepez                         | enrico.yepez@potros.itson.edu.mx                               | Secondary tropical dry forest     | PI                                                           |
| **Alamos Maduro**                               | MX-Aog      | 9 (2016- )                | Enrico A. Yepez                         | enrico.yepez@potros.itson.edu.mx                               | Old growth tropical dry forest    | [AmeriFlux](https://ameriflux.lbl.gov/sites/siteinfo/MX-Aog) |
| **Alamos Secundario**                           | MX-Asf      | 10 (2015- )               | Enrico A. Yepez                         | enrico.yepez@potros.itson.edu.mx                               | Mid-Secondary tropical dry forest | [AmeriFlux](https://ameriflux.lbl.gov/sites/siteinfo/MX-Asf) |
| Bernal                                          | MX-BRN      | 2 (2017-2018)             | Aurelio Guevara-Escobar                 | guevara@uaq.mx                                                 | Secondary xeric shrubland         | [AmeriFlux](https://ameriflux.lbl.gov/sites/siteinfo/MX-BRN) |
| Mexico City-Escandón                            | MX-Esc      | 1.3 (2006, 2011-2012)     | Erik Velasco                            | evelasco@mce2.org                                              | Urban                             | PI                                                           |
| Mexico City-Iztapalapa                          | MX-Izt      | 0.06 (2003)               | Erik Velasco                            | evelasco@mce2.org                                              | Urban                             | PI                                                           |
| Chamela                                         | MX-Cha      | 7.5 (2007-2015)           | Ma. Eugenia González-del Castillo       | eu.gonzaranda@atmosfera.unam.mx                                | Tropical dry forest               | PI                                                           |
| **El Mogor**                                    | MX-EMg      | 15 (2008- )               | Stephen Bullock                         | sbullock@cicese.mx                                             | Mediterranean closed shrubland    | [AmeriFlux](https://ameriflux.lbl.gov/sites/siteinfo/MX-EMg) |
| El Palmar                                       | MX-Pal      | 2.6 (2017-2019)           | Bernardo Figueroa                       | bfigueroae@iingen.unam.mx                                      | Tropical dry forest               | PI, [Zenodo](https://doi.org/10.5281/zenodo.4895662)         |
| El Sargento                                     | MX-Sgt      | 3.5 (2014-2016)           | Julio C. Rodriguez                      | jcrod@guayacan.uson.mx                                         | Short arid mangrove               | [AmeriFlux](https://ameriflux.lbl.gov/sites/siteinfo/MX-Sgt) |
| Encino - Sierra de los Locos                    | MX-Oak      | 3.5 (2008-2013)           | Enrique R. Vivoni, Julio C. Rodriguez   | vivoni@asu.edu, jcrod@guayacan.uson.mx                         | Oak savanna                       | [AmeriFlux](https://ameriflux.lbl.gov/sites/siteinfo/MX-Oak) |
| Esparrago - Hermosillo                          | Mx-Hes      | 2 (2019-2020)             | Julio C. Rodriguez                      | jcrod@guayacan.uson.mx                                         | Crop (asparagus)                  | PI                                                           |
| Estero El Soldado                               | MX-CLS      | 2.5 (2018-2020)           | Zulia M Sanchez-Mejia                   | zulia.sanchez@potros.itson.edu.mx                              | Coastal lagoon                    | [AmeriFlux](https://ameriflux.lbl.gov/sites/siteinfo/MX-CLS) |
| **Instituto de Ingenieria y Tecnologia - UACJ** | MX-lit      | 5 (2020- )                | Eli R. Perez-Ruiz                       | eli.perez@uacj.mx                                              | Urban                             | [AmeriFlux](https://ameriflux.lbl.gov/sites/siteinfo/MX-Iit) |
| **La Colorada - buffel**                        | MX-CHB      | 14 (2011- )               | Alejandro E. Castellanos                | alejandro.castellanos@unison.mx                                | Buffelgrass savanna               | PI                                                           |
| **La Colorada - matorral**                      | MX-CHM      | 12 (2013- )               | Alejandro E. Castellanos                | alejandro.castellanos@unison.mx                                | Subtropical shrubland             | PI                                                           |
| La Encrucijada                                  | MX-Eja      | 0.4 (2014)                | Cristian Tovilla Hernández              | ctovilla@ecosur.mx                                             | Mangrove forest                   | PI                                                           |
| La Mojonera - Atopixco                          | MX-Atx      | 1 (2017)                  | Gregorio Ángeles                        | gangeles@colpos.mx                                             | Mixed oak-pine forest             | PI                                                           |
| La Orduña                                       | MX-Ord      | 6 (2006-2008 & 2014-2018) | Friso Holwerda                          | friso.holwerda@gmail.com                                       | Crop (shaded coffee)              | PI                                                           |
| La Paz                                          | MC-Lpa      | 5 (2001-2005)             | Walter Oechel                           | woechel@mail.sdsu.edu                                          | Desert shrubland                  | [AmeriFlux](https://ameriflux.lbl.gov/sites/siteinfo/MX-Lpa) |
| Monte Xanic                                     | MX-MoX      | 1.5 (2016-2017)           | Stephen Bullock                         | sbullock@cicese.mx                                             | Crop (grape)                      | PI                                                           |
| Navopatia                                       | MX-Nap      | 2 (2017-2019)             | Luis Arturo Méndez Barroso              | luis.mendez@potros.itson.edu.mx                                | Mangrove                          | [Pangaea](https://doi.pangaea.de/10.1594/PANGAEA.905663)     |
| **Nogal - Hermosillo**                          | MX-Hno      | 8 (2017- )                | Julio C. Rodriguez                      | jcrod@guayacan.uson.mx                                         | Perennial crop (walnut)           | PI                                                           |
| Ojuelos                                         | MX-Gra      | 6 (2011-2016)             | Josué Delgado Balbuena                  | josuedlb@gmail.com                                             | Semiarid grassland                | PI                                                           |
| Puerto Morelos                                  | MX-PMm      | 1.4 (2017-2018)           | Susana Alvarado-Barrientos              | susana.alvarado@gmail.com                                      | Basin mangroove                   | [AmeriFlux](https://ameriflux.lbl.gov/sites/siteinfo/MX-PMm) |
| Rayon                                           | MX-Ray      | 5 (2008-2014)             | Chistopher J. Watts, Julio C. Rodriguez | christopher.watts@unison.edu.mx, jcrod@guayacan.uson.mx        | Subtropical shrubland             | [AmeriFlux](https://ameriflux.lbl.gov/sites/siteinfo/MX-Ray) |
| **San Isidro**                                  | MX-ISD      | 2.5 (2019- )              | Dulce Flores-Rentería                   | yaahid.flores@cinvestav.edu.mx                                 | Desert shrubland / Crop           | [AmeriFlux](https://ameriflux.lbl.gov/sites/siteinfo/MX-ISD) |
| Sisal                                           | MX-Sis      | 0.5 (2023)                | Zulia M. Sanchez-Mejia, Jorge Uuh-Sonda | zulia.sanchez@potros.itson.edu.mx, jorge.uuh.sonda@gmail.com   | Basin mangrove                    | PI                                                           |
| Tesopaco                                        | MX-Tes      | 4.5 (2004-2008)           | Enrico A. Yepez, Jaime Garatuza         | enrico.yepez@potros.itson.edu.mx, garatuza@potros.itson.edu.mx | Secondary tropical dry forest     | [AmeriFlux](https://ameriflux.lbl.gov/sites/siteinfo/MX-Tes) |
| Tobari                                          | MX-Tob      | 3 (2014-2016)             | Julio C. Rodriguez                      | jcrod@guayacan.uson.mx                                         | Mangrove                          |                                                              |
| Todos Santos                                    | MX-ToS      | 2 (2008-2009)             |                                         |                                                                | Ocean                             | PI                                                           |
| Valle Del Yaqui                                 | Mx-Yva      | 2 (2013-2014)             | Jaime Garatuza                          | garatuza@potros.itson.edu.mx                                   | Crop (wheat)                      | PI                                                           |
| **Vid - Hermosillo**                            | MX-Hvd      | 7 (2018- )                | Julio C. Rodriguez                      | jcrod@guayacan.uson.mx                                         | Crop (grape)                      | PI                                                           |


